### Teste Globo

Autor: Luis Filipe Guedes Mota

**Banco de Dados**
MongoAtlas

**Backend**
Node.js

**Configuração de Ambiente**
.env

**FrontEnd**
ReactJS

**Execução:**
npm run dev

**Acesso a Aplicação:**
http://localhost:3000