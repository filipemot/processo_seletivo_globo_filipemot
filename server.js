const express = require('express');
require('dotenv/config');
const app = express()
const bodyParser = require('body-parser');

const ObjectId = require('mongodb').ObjectID
const MongoClient = require('mongodb').MongoClient

const uri = process.env.MONGO_URL;
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: true }));

MongoClient.connect(uri,{useUnifiedTopology: true}).then(client => {

  db = client.db('globo');

  app.listen(5000, () => {
    console.log('Server running on port 5000');
  });
}).catch(err =>console.log(err));

app.get('/api/noticia', (req, res) => {
    db.collection('noticia').find().toArray((err, results) => {
        if (err) return console.log(err);
        res.send(results);
      });
});

app.get('/api/noticia/:id', (req, res) => {
  let id = req.params.id;
  db.collection('noticia').find(ObjectId(id)).toArray((err, result) => {
      if (err) return console.log(err);
      res.send(result);
    });
});


app.post('/api/noticia', (req, res) => {

    db.collection('noticia').insertOne(req.body, (err, result) => {
        if (err) return console.log(err);

        console.log('Salvo no Banco de Dados')
        res.send({});
      });
});

app.put('/api/noticia/:id', (req, res) => {
    let id = req.params.id;
    let titulo = req.body.titulo;
    let dataPublicacao = req.body.dataPublicacao;
    let conteudo = req.body.conteudo;

    db.collection('noticia').updateOne({_id: ObjectId(id)}, {
      $set: {
        titulo: titulo,
        dataPublicacao: dataPublicacao,
        conteudo: conteudo
      }
    }, (err, result) => {
      if (err) return res.send(err)
      res.send({});
      console.log('Atualizado no Banco de Dados')
    });
});

app.delete('/api/noticia/:id', (req, res) => {
    let id = req.params.id

    db.collection('noticia').deleteOne({_id: ObjectId(id)}, (err, result) => {
      if (err) return res.send(500, err)
      console.log('Deletado do Banco de Dados!')
      res.send({});
    });
});