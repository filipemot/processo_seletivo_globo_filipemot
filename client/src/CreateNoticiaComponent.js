import React, { Component } from 'react'
import NoticiaService from './services/NoticiaService';

class CreateNoticiaComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
            // step 2
            id: this.props.match.params.id,
            titulo: '',
            Conteudo: '',
            dataPublicacao: ''
        }
        this.changetituloHandler = this.changetituloHandler.bind(this);
        this.changeConteudoHandler = this.changeConteudoHandler.bind(this);
        this.saveOrUpdateNoticia = this.saveOrUpdateNoticia.bind(this);
    }

    // step 3
    componentDidMount(){

        // step 4
        if(this.state.id === '_add'){
            return;
        }else{
            NoticiaService.getNoticiaById(this.state.id).then( (res) =>{
                let Noticia;
                if(res.data.length>0){
                    Noticia = res.data[0];
                } else {
                    Noticia = {};
                }

                this.setState({titulo: Noticia.titulo,
                    conteudo: Noticia.conteudo,
                    dataPublicacao : Noticia.dataPublicacao
                });
            });
        }
    }
    saveOrUpdateNoticia = (e) => {
        e.preventDefault();
        let Noticia = {titulo: this.state.titulo, conteudo: this.state.conteudo, dataPublicacao: this.state.dataPublicacao};
        console.log('Noticia => ' + JSON.stringify(Noticia));

        // step 5
        if(this.state.id === '_add'){
            NoticiaService.createNoticia(Noticia).then(res =>{
                this.props.history.push('/Noticias');
            });
        }else{
            NoticiaService.updateNoticia(Noticia, this.state.id).then( res => {
                this.props.history.push('/Noticias');
            });
        }
    }

    changetituloHandler= (event) => {
        this.setState({titulo: event.target.value});
    }

    changeConteudoHandler= (event) => {
        this.setState({conteudo: event.target.value});
    }

    changePublicacaoHandler= (event) => {
        this.setState({dataPublicacao: event.target.value});
    }

    cancel(){
        this.props.history.push('/Noticias');
    }

    getTitle(){
        if(this.state.id === '_add'){
            return <h3 className="text-center">Add Noticia</h3>
        }else{
            return <h3 className="text-center">Update Noticia</h3>
        }
    }
    render() {
        return (
            <div>
                <br></br>
                   <div className = "container">
                        <div className = "row">
                            <div className = "card col-md-6 offset-md-3 offset-md-3">
                                {
                                    this.getTitle()
                                }
                                <div className = "card-body">
                                    <form>
                                        <div className = "form-group">
                                            <label> Titulo: </label>
                                            <input placeholder="Titulo" name="titulo" className="form-control"
                                                value={this.state.titulo} onChange={this.changetituloHandler}/>
                                        </div>
                                        <div className = "form-group">
                                            <label> Conteudo: </label>
                                            <input placeholder="Conteudo" name="conteudo" className="form-control"
                                                value={this.state.conteudo} onChange={this.changeConteudoHandler}/>
                                        </div>
                                        <div className = "form-group">
                                            <label> Data da publicação: </label>
                                            <input type="date" placeholder="Data da Publicação" name="dataPublicacao" className="form-control"
                                                value={this.state.dataPublicacao} onChange={this.changePublicacaoHandler}/>
                                        </div>

                                        <button className="btn btn-success" onClick={this.saveOrUpdateNoticia}>Save</button>
                                        <button className="btn btn-danger" onClick={this.cancel.bind(this)} style={{marginLeft: "10px"}}>Cancel</button>
                                    </form>
                                </div>
                            </div>
                        </div>

                   </div>
            </div>
        )
    }
}

export default CreateNoticiaComponent