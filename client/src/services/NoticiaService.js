import axios from 'axios';

const NOTICIA_API_BASE_URL = "/api/noticia";

class NoticiaService {

    getNoticias(){
        return axios.get(NOTICIA_API_BASE_URL);
    }

    createNoticia(Noticia){

        const params = JSON.stringify(Noticia);

        return axios.post(NOTICIA_API_BASE_URL,params,{

            "headers": {

            "content-type": "application/json",

            }});
    }

    getNoticiaById(NoticiaId){
        return axios.get(NOTICIA_API_BASE_URL + '/' + NoticiaId);
    }

    updateNoticia(Noticia, NoticiaId){
        return axios.put(NOTICIA_API_BASE_URL + '/' + NoticiaId, Noticia);
    }

    deleteNoticia(NoticiaId){
        return axios.delete(NOTICIA_API_BASE_URL + '/' + NoticiaId);
    }
}

export default new NoticiaService()