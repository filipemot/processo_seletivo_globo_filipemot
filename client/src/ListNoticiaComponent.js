import React, { Component } from 'react'
import NoticiaService from './services/NoticiaService'

class ListNoticiaComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
                Noticias: []
        }
        this.addNoticia = this.addNoticia.bind(this);
        this.editNoticia = this.editNoticia.bind(this);
        this.deleteNoticia = this.deleteNoticia.bind(this);
    }

    deleteNoticia(id){
        NoticiaService.deleteNoticia(id).then( res => {
            this.setState({Noticias: this.state.Noticias.filter(Noticia => Noticia._id !== id)});
        });
    }
    viewNoticia(id){
        this.props.history.push(`/view-Noticia/${id}`);
    }
    editNoticia(id){
        this.props.history.push(`/add-Noticia/${id}`);
    }

    componentDidMount(){
        NoticiaService.getNoticias().then((res) => {
            this.setState({ Noticias: res.data});
        });
    }

    addNoticia(){
        this.props.history.push('/add-Noticia/_add');
    }

    render() {
        return (
            <div>
                 <h2 className="text-center">Noticias List</h2>
                 <div className = "row">
                    <button className="btn btn-primary" onClick={this.addNoticia}> Add Noticia</button>
                 </div>
                 <br></br>
                 <div className = "row">
                        <table className = "table table-striped table-bordered">

                            <thead>
                                <tr>
                                    <th> Noticia Titulo</th>
                                    <th> Noticia Conteudo</th>
                                    <th> Noticia Data</th>
                                    <th> Acao</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.Noticias.map(
                                        Noticia =>
                                        <tr key = {Noticia.id}>
                                             <td> {Noticia.titulo} </td>
                                             <td> {Noticia.conteudo}</td>
                                             <td> {Noticia.dataPublicacao}</td>
                                             <td>
                                                 <button onClick={ () => this.editNoticia(Noticia._id)} className="btn btn-info">Update </button>
                                                 <button style={{marginLeft: "10px"}} onClick={ () => this.deleteNoticia(Noticia._id)} className="btn btn-danger">Delete </button>
                                                 <button style={{marginLeft: "10px"}} onClick={ () => this.viewNoticia(Noticia._id)} className="btn btn-info">View </button>
                                             </td>
                                        </tr>
                                    )
                                }
                            </tbody>
                        </table>

                 </div>

            </div>
        )
    }
}

export default ListNoticiaComponent