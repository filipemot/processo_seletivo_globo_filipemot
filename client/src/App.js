import React from 'react';
import './App.css';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import HeaderComponent from './HeaderComponent';
import FooterComponent from './FooterComponent';
import ListNoticiaComponent from './ListNoticiaComponent';
import CreateNoticiaComponent from './CreateNoticiaComponent';
import ViewNoticiaComponent from './ViewNoticiaComponent';

function App() {
  return (
    <div>
        <Router>
              <HeaderComponent />
                <div className="container">
                    <Switch>
                          <Route path = "/" exact component = {ListNoticiaComponent}></Route>
                          <Route path = "/Noticias" component = {ListNoticiaComponent}></Route>
                          <Route path = "/add-noticia/:id" component = {CreateNoticiaComponent}></Route>
                          <Route path = "/view-noticia/:id" component = {ViewNoticiaComponent}></Route>
                    </Switch>
                </div>
              <FooterComponent />
        </Router>
    </div>

  );
}

export default App;