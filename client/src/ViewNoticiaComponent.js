import React, { Component } from 'react'
import NoticiaService from './services/NoticiaService'

class ViewNoticiaComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
            id: this.props.match.params.id,
            Noticia: {}
        }
    }

    componentDidMount(){
        NoticiaService.getNoticiaById(this.state.id).then( res => {
            if(res.data.length>0){
                this.setState({Noticia: res.data[0]});
            } else {
                this.setState({Noticia: {}});
            }
        });
    }

    render() {
        return (
            <div>
                <br></br>
                <div className = "card col-md-6 offset-md-3">
                    <h3 className = "text-center"> View Noticia Details</h3>
                    <div className = "card-body">
                        <div className = "row">
                            <label> Titulo: </label>
                            <div> { this.state.Noticia.titulo }</div>
                        </div>
                        <div className = "row">
                            <label> Conteudo: </label>
                            <div> { this.state.Noticia.conteudo }</div>
                        </div>
                        <div className = "row">
                            <label> Data de Publicação: </label>
                            <div> { this.state.Noticia.dataPublicacao }</div>
                        </div>
                    </div>

                </div>
            </div>
        )
    }
}

export default ViewNoticiaComponent